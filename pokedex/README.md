## UKFast technical Test

###Summary

My attempt at matching the design criteria, for a web based encyclopedia based on the popular franchise Pokémon

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Project has been deployed at https://distracted-bell-ec2a0f.netlify.com/

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

## Improvements

If i had more time i would include type checking with prop types.<br />
Split CSS into seperate modules, and include SCSS.<br />
Implement Testing

## Issues

After forking the repo, i was unable to install any of the dependancies.<br />
Was unable to use any of the tasks within the gulp file, specifically 'gulp sass'

Created by Jamie Davis <br />
17/02/2020<br />

Any questions, feel free to email me at <br /> jamiebarrydavis@gmail.com